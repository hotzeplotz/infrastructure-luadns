-- File: boredom.is.lua
-- Zone: boredom.is
-- SOA record is automatically generated
-- Variable _a is replaced with zone name
-- _a = "boredom.is"

-- ALIAS records
alias(_a, "higher-order-boredom.gitlab.io")

-- MX records
mx(_a, "mail.0x0b.net", 10)

-- NS records
ns(_a, "ns1.luadns.net", 86400)
ns(_a, "ns2.luadns.net", 86400)
ns(_a, "ns3.luadns.net", 86400)
ns(_a, "ns4.luadns.net", 86400)
ns(_a, "ns5.luadns.net", 86400)

-- CNAME records
cname("www", _a, 300)

-- SPF records
spf(_a, "v=spf1 ip4:185.67.36.0/23 ip4:89.146.220.128/29 ip4:89.146.194.160/28 ip4:89.146.230.64/26 ip4:213.138.103.117 ip6:2001:41c8:51:275::10 ~all", 300)

